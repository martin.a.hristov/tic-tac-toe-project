# Use Alpine Linux as the base image
FROM node:lts AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:alpine AS runtime

COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80